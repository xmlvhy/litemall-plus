package org.linlinjava.litemall.wx.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.core.validator.Order;
import org.linlinjava.litemall.core.validator.Sort;
import org.linlinjava.litemall.db.domain.LitemallBrand;
import org.linlinjava.litemall.db.domain.LitemallGoods;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.linlinjava.litemall.db.service.LitemallBrandService;
import org.linlinjava.litemall.db.service.LitemallUserService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.dto.GoodsAllinone;
import org.linlinjava.litemall.wx.service.WxGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 品牌
 */
@RestController
@RequestMapping("/wx/brand")
@Validated
public class WxBrandController {
    private final Log logger = LogFactory.getLog(WxBrandController.class);

    @Autowired
    private LitemallBrandService brandService;
    @Autowired
    private LitemallUserService userService;
    @Autowired
    private WxGoodsService wxGoodsService;

    /**
     * 品牌列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 品牌列表
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<LitemallBrand> brandList = brandService.query(page, limit, sort, order);
        return ResponseUtil.okList(brandList);
    }

    private Object validate(LitemallBrand brand) {
        String name = brand.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }

        String desc = brand.getDesc();
        if (StringUtils.isEmpty(desc)) {
            return ResponseUtil.badArgument();
        }

        String picUrl = brand.getPicUrl();
        if (StringUtils.isEmpty(picUrl)){
            return ResponseUtil.badArgument();
        }

        BigDecimal price = brand.getFloorPrice();
        if (price == null) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    /**
     * 品牌详情
     *
     * @param brandId 品牌ID
     * @return 品牌详情
     */
    @GetMapping("detail")
    public Object brandDetail(@NotNull Integer brandId) {
        LitemallBrand brand = brandService.findById(brandId);
        if (brand == null) {
            return ResponseUtil.badArgumentValue();
        }
        Integer userId = brand.getUserId();
        HashMap<String, Object> dataMap = new HashMap<>();
        dataMap.put("brand",brand);
        if (userId != null){
            dataMap.put("brandUser",userService.findById(userId));
        }
        return ResponseUtil.ok(dataMap);
    }


    /**
     * 添加或修改店铺
     * @param userId
     * @param brand
     * @return
     */
    @PostMapping("/save")
    public Object create(@LoginUser Integer userId ,@RequestBody LitemallBrand brand) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Object error = validate(brand);
        if (error != null) {
            return error;
        }
        List<LitemallBrand> brands = brandService.queryByUserId(userId);
        if (brands.size() > 0){
            if (!userId.equals(brand.getUserId())){
                return ResponseUtil.fail(700,"无权限");
            }
            if(brand.getId() == null){
                return ResponseUtil.fail(700,"无权限");
            }
            //更新店铺信息
            if (brandService.updateById(brand) == 0) {
                return ResponseUtil.updatedDataFailed();
            }
        }else {
            brand.setUserId(userId);
            brandService.add(brand);
        }

        return ResponseUtil.ok(brand);
    }



    //#############################################################
    //------------------TODO 以下是店铺商品管理接口
    //#############################################################


    @GetMapping("/goods/category")
    public Object catList() {
        return wxGoodsService.catList();
    }

    /**
     * 编辑商品
     * @param goodsAllinone
     * @return
     */
    @PostMapping("/goods/update")
    public Object update(@RequestBody GoodsAllinone goodsAllinone) {
        return wxGoodsService.update(goodsAllinone);
    }

    /**
     * 删除商品
     * @param goods
     * @return
     */
    @PostMapping("/goods/delete")
    public Object delete(@RequestBody LitemallGoods goods) {
        return wxGoodsService.delete(goods);
    }

    /**
     * 添加商品
     * @param goodsAllinone
     * @return
     */
    @PostMapping("/goods/create")
    public Object create(@RequestBody GoodsAllinone goodsAllinone) {
        return wxGoodsService.create(goodsAllinone);
    }

    /**
     * 商品详情
     * @param id
     * @return
     */
    @GetMapping("/goods/detail")
    public Object goodDetail(@NotNull Integer id) {
        return wxGoodsService.detail(id);
    }

}