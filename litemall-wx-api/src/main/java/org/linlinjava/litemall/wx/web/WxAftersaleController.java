package org.linlinjava.litemall.wx.web;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.service.NotifyService;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.core.validator.Order;
import org.linlinjava.litemall.core.validator.Sort;
import org.linlinjava.litemall.db.domain.*;
import org.linlinjava.litemall.db.service.*;
import org.linlinjava.litemall.db.util.AftersaleConstant;
import org.linlinjava.litemall.db.util.OrderUtil;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.util.WxResponseCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 售后服务
 *
 * 目前只支持订单整体售后，不支持订单商品单个售后
 *
 * 一个订单只能有一个售后记录
 */
@RestController
@RequestMapping("/wx/aftersale")
@Validated
public class WxAftersaleController {
    private final Log logger = LogFactory.getLog(WxAftersaleController.class);

    @Autowired
    private LitemallAftersaleService aftersaleService;
    @Autowired
    private LitemallOrderService orderService;
    @Autowired
    private LitemallOrderGoodsService orderGoodsService;
    @Autowired
    private NotifyService notifyService;

    /**
     * 售后列表
     *
     * @param userId   用户ID
     * @param status   状态类型，如果是空则是全部
     * @param page     分页页数
     * @param limit    分页大小
     * @param sort     排序字段
     * @param order    排序方式
     * @return 售后列表
     */
    @GetMapping("list")
    public Object list(@LoginUser Integer userId,
                       @RequestParam Short status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        List<LitemallAftersale> aftersaleList = aftersaleService.queryList(userId, status, page, limit, sort, order);

        List<Map<String, Object>> aftersaleVoList = new ArrayList<>(aftersaleList.size());
        for (LitemallAftersale aftersale : aftersaleList) {
            List<LitemallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(aftersale.getOrderId());

            Map<String, Object> aftersaleVo = new HashMap<>();
            aftersaleVo.put("aftersale", aftersale);
            aftersaleVo.put("goodsList", orderGoodsList);

            aftersaleVoList.add(aftersaleVo);
        }

        return ResponseUtil.okList(aftersaleVoList, aftersaleList);
    }

    /**
     * 售后详情
     *
     * @param orderId 订单ID
     * @return 售后详情
     */
    @GetMapping("detail")
    public Object detail(@LoginUser Integer userId, @NotNull Integer orderId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        LitemallOrder order = orderService.findById(userId, orderId);
        if (order == null){
            return ResponseUtil.badArgumentValue();
        }
        List<LitemallOrderGoods> orderGoodsList = orderGoodsService.queryByOid(orderId);
        LitemallAftersale aftersale = aftersaleService.findByOrderId(userId, orderId);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("aftersale", aftersale);
        data.put("order", order);
        data.put("orderGoods", orderGoodsList);
        return ResponseUtil.ok(data);
    }

    /**
     * 申请售后
     *
     * @param userId   用户ID
     * @param aftersale 用户售后信息
     * @return 操作结果
     */
    @PostMapping("submit")
    public Object submit(@LoginUser Integer userId, @RequestBody LitemallAftersale aftersale) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Object error = validate(aftersale);
        if (error != null) {
            return error;
        }
        // 进一步验证
        Integer orderId = aftersale.getOrderId();
        if(orderId == null){
            return ResponseUtil.badArgument();
        }
        LitemallOrder order = orderService.findById(userId, orderId);
        if(order == null){
            return ResponseUtil.badArgumentValue();
        }

        // 订单必须完成才能进入售后流程。
        if(!OrderUtil.isConfirmStatus(order) && !OrderUtil.isAutoConfirmStatus(order)){
            return ResponseUtil.fail(WxResponseCode.AFTERSALE_UNALLOWED, "不能申请售后");
        }
        BigDecimal amount = order.getActualPrice().subtract(order.getFreightPrice());
        if(aftersale.getAmount().compareTo(amount) > 0){
            return ResponseUtil.fail(WxResponseCode.AFTERSALE_INVALID_AMOUNT, "退款金额不正确");
        }
        Short afterStatus = order.getAftersaleStatus();
        if(afterStatus.equals(AftersaleConstant.STATUS_RECEPT) || afterStatus.equals(AftersaleConstant.STATUS_REFUND)){
            return ResponseUtil.fail(WxResponseCode.AFTERSALE_INVALID_AMOUNT, "已申请售后");
        }

        // 如果有旧的售后记录则删除（例如用户已取消，管理员拒绝）
        aftersaleService.deleteByOrderId(userId, orderId);
        aftersale.setStatus(AftersaleConstant.STATUS_REQUEST);
        aftersale.setAftersaleSn(aftersaleService.generateAftersaleSn(userId));
        aftersale.setUserId(userId);
        aftersaleService.add(aftersale);

        //修改订单状态
        order.setOrderStatus(OrderUtil.STATUS_PUT_AFTERSALE);
        orderService.updateWithOptimisticLocker(order);

        // 订单的aftersale_status和售后记录的status是一致的。
        orderService.updateAftersaleStatus(orderId, AftersaleConstant.STATUS_REQUEST);

        //TODO 发送邮件和短信通知，这里采用异步发送
        // 有用户申请退款，邮件通知运营人员
        StringBuilder inform = new StringBuilder("<html><head></head><body>");
        inform.append("<table border='1' style='font-size:15px;width:100%;'>");
        inform.append("<thead>");
        inform.append("<th style='padding:15px;'>" + "售后详情" + "</th>");
        inform.append("</thead>");
        inform.append("</table>");

        inform.append("<table border='1' style='font-size:15px;width:100%;'>");
        inform.append("<tr>");
        inform.append("<td style='padding:15px;width: 65px;'>" + "订单编号:" + "</td>");
        inform.append("<td style='padding:15px;'>" + order.getOrderSn() + "</td>");
        inform.append("</tr>");
        if(!"".equals(order.getConsignee()) && !"无".equals(order.getConsignee())){
            inform.append("<tr>");
            inform.append("<td style='padding:15px;'>" + "退款用户:" + "</td>");
            inform.append("<td style='padding:15px;'>" + order.getConsignee() + "</td>");
            inform.append("</tr>");
            inform.append("<tr>");
            inform.append("<td style='padding:15px;'>" + "联系电话:" + "</td>");
            inform.append("<td style='padding:15px;'>" + order.getMobile() + "</td>");
            inform.append("</tr>");
        }

        List<LitemallOrderGoods> orderGoods = orderGoodsService.queryByOid(order.getId());
        for (LitemallOrderGoods litemallOrderGoods : orderGoods) {
            String [] getSpecifications = litemallOrderGoods.getSpecifications();

            StringBuffer specifications = new StringBuffer();
            for (int i = 0; i < getSpecifications.length; i++) {
                specifications.append("["+getSpecifications[i]+"] ");
            }
            inform.append("<tr>");
            inform.append("<td style='padding:15px;color: rgb(0, 10, 255);'>" + "商品名称:" + "</td>");
            inform.append("<td style='padding:15px;color: rgb(0, 10, 255);'>" + litemallOrderGoods.getGoodsName() + "</td>");
            inform.append("</tr>");
            inform.append("<tr>");
            inform.append("<td style='padding:15px;color: rgb(0, 164, 255);'>" + "商品规格:" + "</td>");
            inform.append("<td style='padding:15px;color: rgb(0, 164, 255);'>" + specifications.toString() + "</td>");
            inform.append("</tr>");
            inform.append("<tr>");
            inform.append("<td style='padding:15px;color: rgb(0, 164, 255);'>" + "商品数量:" + "</td>");
            inform.append("<td style='padding:15px;color: rgb(0, 164, 255);'>" + "x"+litemallOrderGoods.getNumber() + "</td>");
            inform.append("</tr>");
        }
        inform.append("<tr>");
        inform.append("<td style='padding:15px;color: rgb(255, 0, 0);'>" + "退款金额:" + "</td>");
        inform.append("<td style='padding:15px;color: rgb(255, 0, 0);'>" + aftersale.getAmount()+"元" + "</td>");
        inform.append("</tr>");
        inform.append("<tr>");
        inform.append("<td style='padding:15px;'>" + "退款类型:" + "</td>");
        if (aftersale.getType() == 0){
            inform.append("<td style='padding:15px;'>" + "未收货退款" + "</td>");
        }else if (aftersale.getType() == 1){
            inform.append("<td style='padding:15px;'>" + "已收货（无需退货）" + "</td>");
        }else if (aftersale.getType() == 2){
            inform.append("<td style='padding:15px;'>" + "退货退款" + "</td>");
        }
        inform.append("</tr>");
        inform.append("<tr>");
        inform.append("<td style='padding:15px;'>" + "退款备注:" + "</td>");
        inform.append("<td style='padding:15px;'>" + aftersale.getReason() + "</td>");
        inform.append("</tr>");
        inform.append("<tr>");
        inform.append("<td style='padding:15px;'>" + "申请日期:" + "</td>");
        inform.append("<td style='padding:15px;'>" + LocalDateTime.now() + "</td>");
        inform.append("</tr>");
        inform.append("</table>");

        inform.append("<p>");
        inform.append("<a href=\"http://www.makbk.com\" style='font-size: 16px; line-height: 45px; display: block; background-color: rgb(0, 164, 255); color: rgb(255, 255, 255); text-align: center; text-decoration: none; margin-top: 20px; border-radius: 3px;'>去查看</a>");
        inform.append("</p>");

        inform.append("</body></html>");

        notifyService.notifyMail("售后申请", inform.toString());


        return ResponseUtil.ok();
    }

    /**
     * 取消售后
     *
     * 如果管理员还没有审核，用户可以取消自己的售后申请
     *
     * @param userId   用户ID
     * @param aftersale 用户售后信息
     * @return 操作结果
     */
    @PostMapping("cancel")
    public Object cancel(@LoginUser Integer userId, @RequestBody LitemallAftersale aftersale) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }
        Integer id = aftersale.getId();
        if(id == null){
            return ResponseUtil.badArgument();
        }
        LitemallAftersale aftersaleOne = aftersaleService.findById(userId, id);
        if(aftersaleOne == null){
            return ResponseUtil.badArgument();
        }

        Integer orderId = aftersaleOne.getOrderId();
        LitemallOrder order = orderService.findById(userId, orderId);
        if(!order.getUserId().equals(userId)){
            return ResponseUtil.badArgumentValue();
        }

        // 订单必须完成才能进入售后流程。
        if(!OrderUtil.isConfirmStatus(order) && !OrderUtil.isAutoConfirmStatus(order)){
            return ResponseUtil.fail(WxResponseCode.AFTERSALE_UNALLOWED, "不支持售后");
        }
        Short afterStatus = order.getAftersaleStatus();
        if(!afterStatus.equals(AftersaleConstant.STATUS_REQUEST)){
            return ResponseUtil.fail(WxResponseCode.AFTERSALE_INVALID_STATUS, "不能取消售后");
        }

        aftersale.setStatus(AftersaleConstant.STATUS_CANCEL);
        aftersale.setUserId(userId);
        aftersaleService.updateById(aftersale);

        // 订单的aftersale_status和售后记录的status是一致的。
        orderService.updateAftersaleStatus(orderId, AftersaleConstant.STATUS_CANCEL);
        return ResponseUtil.ok();
    }

    private Object validate(LitemallAftersale aftersale) {
        Short type = aftersale.getType();
        if (type == null) {
            return ResponseUtil.badArgument();
        }
        BigDecimal amount = aftersale.getAmount();
        if (amount == null) {
            return ResponseUtil.badArgument();
        }
        String reason = aftersale.getReason();
        if (StringUtils.isEmpty(reason)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }
}