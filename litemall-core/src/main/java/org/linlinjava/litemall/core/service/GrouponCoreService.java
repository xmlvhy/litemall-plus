package org.linlinjava.litemall.core.service;

import com.github.binarywang.wxpay.bean.notify.WxPayNotifyResponse;
import org.linlinjava.litemall.core.util.NotifyMessageUtil;
import org.linlinjava.litemall.db.domain.LitemallGroupon;
import org.linlinjava.litemall.db.domain.LitemallGrouponRules;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;
import org.linlinjava.litemall.db.service.LitemallGrouponRulesService;
import org.linlinjava.litemall.db.service.LitemallGrouponService;
import org.linlinjava.litemall.db.service.LitemallOrderGoodsService;
import org.linlinjava.litemall.db.service.LitemallOrderService;
import org.linlinjava.litemall.db.util.GrouponConstant;
import org.linlinjava.litemall.db.util.OrderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GrouponCoreService {

    @Autowired
    private LitemallOrderService orderService;
    @Autowired
    private QCodeService qCodeService;
    @Autowired
    private LitemallGrouponRulesService grouponRulesService;
    @Autowired
    private LitemallGrouponService grouponService;
    @Autowired
    private LitemallOrderGoodsService orderGoodsService;
    @Autowired
    private NotifyService notifyService;

    /**
     * 修改团购状态和订单状态
     * @param groupon
     */
    public void updateGrouponStatus(LitemallGroupon groupon){
        //获取团购规则信息
        LitemallGrouponRules grouponRules = grouponRulesService.findById(groupon.getRulesId());
        //仅当发起者才创建分享图片
        if (groupon.getGrouponId() == 0) {
            String goodsName = grouponRules.getGoodsName();
            String picUrl = grouponRules.getPicUrl();
            String url = qCodeService.createGrouponShareImage(goodsName, picUrl, groupon);
            groupon.setShareUrl(url);
        }

        //获取团购参与信息
        List<LitemallGroupon> grouponList = grouponService.queryJoinRecord(groupon.getGrouponId());
        if (groupon.getGrouponId() != 0 && (grouponList.size() >= grouponRules.getDiscountMember() - 1)) {
            for (LitemallGroupon grouponActivity : grouponList) {
                //修改团购状态
                grouponActivity.setStatus(GrouponConstant.STATUS_SUCCEED);
                grouponService.updateById(grouponActivity);

                //修改订单团购状态
                LitemallOrder litemallOrder = new LitemallOrder();
                litemallOrder.setId(grouponActivity.getOrderId());
                litemallOrder.setOrderStatus(OrderUtil.STATUS_GROUPON_SUCCEED);
                orderService.updateSelective(litemallOrder);

                //给商家发送邮件通知
                LitemallOrder grouponOrder = orderService.findById(grouponActivity.getOrderId());
                List<LitemallOrderGoods> orderGoods = orderGoodsService.queryByOid(grouponOrder.getId());
                String orderMessage = NotifyMessageUtil.orderMessage(grouponOrder, orderGoods);
                notifyService.notifyMail("新订单通知", orderMessage);
            }
            //修改团购状态
            LitemallGroupon grouponSource = grouponService.queryById(groupon.getGrouponId());
            grouponSource.setStatus(GrouponConstant.STATUS_SUCCEED);
            grouponService.updateById(grouponSource);

            //修改订单团购状态
            LitemallOrder litemallOrder = new LitemallOrder();
            litemallOrder.setId(grouponSource.getOrderId());
            litemallOrder.setOrderStatus(OrderUtil.STATUS_GROUPON_SUCCEED);
            orderService.updateSelective(litemallOrder);

            //给商家发送邮件通知
            LitemallOrder grouponOrder = orderService.findById(grouponSource.getOrderId());
            List<LitemallOrderGoods> orderGoods = orderGoodsService.queryByOid(grouponOrder.getId());
            String orderMessage = NotifyMessageUtil.orderMessage(grouponOrder, orderGoods);
            notifyService.notifyMail("新订单通知", orderMessage);
        }else {
            //修改团购状态
            groupon.setStatus(GrouponConstant.STATUS_ON);
            grouponService.updateById(groupon);

            //修改订单团购状态
            LitemallOrder litemallOrder = new LitemallOrder();
            litemallOrder.setId(groupon.getOrderId());
            litemallOrder.setOrderStatus(OrderUtil.STATUS_GROUPON_ON);
            orderService.updateSelective(litemallOrder);
        }
    }
}
