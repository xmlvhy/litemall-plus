package org.linlinjava.litemall.core.service;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.config.WxProperties;
import org.linlinjava.litemall.core.express.ExpressService;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.util.OrderUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;

@Service
public class SubscribeMessageService {

    private final Log logger = LogFactory.getLog(SubscribeMessageService.class);

    @Autowired
    private WxProperties properties;
    @Autowired
    private ExpressService expressService;
    //声明需要格式化的格式(日期加时间)
    private DateTimeFormatter dfDateTime = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm");

    public String getAccessToken() {
        String appId = properties.getAppId();
        String appSecret = properties.getAppSecret();
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + appSecret;
        String result = HttpUtil.get(url);
        JSONObject jsonObject = JSONUtil.parseObj(result);
        return jsonObject.getStr("access_token");
    }


    /**
     * 订单发货订阅
     * @param openId
     * @param order
     */
    public void shipSubscribe(String openId ,LitemallOrder order){
        JSONObject body = new JSONObject();
        body.set("touser",openId);
        body.set("template_id",properties.getShipTmplId());
        //拼接模板json
        JSONObject json=new JSONObject();
        json.set("character_string1",new JSONObject().set("value",order.getOrderSn()));
        json.set("date3",new JSONObject().set("value", dfDateTime.format(order.getShipTime())));
        json.set("thing7",new JSONObject().set("value",expressService.getVendorName(order.getShipChannel())));
        json.set("character_string9",new JSONObject().set("value", order.getShipSn()));
        json.set("thing15",new JSONObject().set("value", order.getAddress()));
        body.set("data",json);
        //发送
        String accessToken= getAccessToken();
        String result = HttpUtil.post("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken, body.toString());
        JSONObject jsonObject = JSONUtil.parseObj(result);
        if (!jsonObject.getStr("errcode").equals("0")){
            logger.info("消息订阅错误信息-------发货订阅-----错误信息：" + result);
        }
    }


    /**
     * 订单退货审核订阅通知
     * @param openId
     * @param order
     */
    public void refundSubscribe(String openId ,LitemallOrder order){
        JSONObject body = new JSONObject();
        body.set("touser",openId);
        body.set("template_id",properties.getRefundTmplId());
        //拼接模板json
        JSONObject json=new JSONObject();
        json.set("thing1",new JSONObject().set("value", OrderUtil.orderStatusText(order)));
        json.set("character_string2",new JSONObject().set("value", order.getOrderSn()));
        json.set("amount3",new JSONObject().set("value",order.getRefundAmount()));
        json.set("time11",new JSONObject().set("value", dfDateTime.format(order.getPayTime())));
        json.set("date4",new JSONObject().set("value", dfDateTime.format(order.getRefundTime())));
        body.set("data",json);
        //发送
        String accessToken= getAccessToken();
        String result = HttpUtil.post("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken, body.toString());
        JSONObject jsonObject = JSONUtil.parseObj(result);
        if (!jsonObject.getStr("errcode").equals("0")){
            logger.info("消息订阅错误信息-------退款订阅-----错误信息：" + result);
        }
    }

}
