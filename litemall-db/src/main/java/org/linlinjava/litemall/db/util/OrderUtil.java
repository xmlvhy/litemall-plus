package org.linlinjava.litemall.db.util;

import org.linlinjava.litemall.db.domain.LitemallOrder;

import java.util.ArrayList;
import java.util.List;

/*
 * 订单流程：下单成功－》支付订单－》发货－》收货
 * 订单状态：
 * 101 订单生成，未支付；102，下单未支付用户取消；103，下单未支付超期系统自动取消
 * 201 支付完成，商家未发货；202，订单生产，已付款未发货，用户申请退款；203，管理员执行退款操作，确认退款成功；
 * 301 商家发货，用户未确认；
 * 302 用户确认收货，订单结束； 303 用户没有确认收货，但是快递反馈已收货后，超过一定时间，系统自动确认收货，订单结束。
 *
 * 当101用户未付款时，此时用户可以进行的操作是取消或者付款
 * 当201支付完成而商家未发货时，此时用户可以退款
 * 当301商家已发货时，此时用户可以有确认收货
 * 当302用户确认收货以后，此时用户可以进行的操作是退货、删除、去评价或者再次购买
 * 当303系统自动确认收货以后，此时用户可以删除、去评价、或者再次购买
 *
 */
public class OrderUtil {

    /**未付款*/
    public static final Short STATUS_CREATE = 101;
    /**已取消*/
    public static final Short STATUS_CANCEL = 102;
    /**已取消(系统)*/
    public static final Short STATUS_AUTO_CANCEL = 103;
    /**已取消(管理员)*/
    public static final Short STATUS_ADMIN_CANCEL = 104;

    /**已付款*/
    public static final Short STATUS_PAY = 201;
    /**订单取消，退款中*/
    public static final Short STATUS_REFUND = 202;
    /**已退款*/
    public static final Short STATUS_REFUND_CONFIRM = 203;

    /**待开团（未支付）*/
    public static final Short STATUS_GROUPON_NONE = 301;
    /**团购中（已支付）*/
    public static final Short STATUS_GROUPON_ON = 302;
    /**团购失败（待退款）*/
    public static final Short STATUS_GROUPON_FAIL = 303;
    /**团购成功（待发货）*/
    public static final Short STATUS_GROUPON_SUCCEED = 304;

    /**已发货*/
    public static final Short STATUS_SHIP = 401;
    /**已收货*/
    public static final Short STATUS_CONFIRM = 402;
    /**已收货(系统)*/
    public static final Short STATUS_AUTO_CONFIRM = 403;
    /**评价已超时*/
    public static final Short STATUS_COMMENT_OVERTIME = 404;
    /**交易完成*/
    public static final Short STATUS_COMMENT = 405;

    /**售后申请中*/
    public static final Short STATUS_PUT_AFTERSALE = 601;
    /**售后退款中*/
    public static final Short STATUS_DISPOSE_AFTERSALE = 602;
    /**售后已完成*/
    public static final Short STATUS_FINISH_AFTERSALE = 603;
    /**售后已拒绝*/
    public static final Short STATUS_REJECT_AFTERSALE = 604;

    public static String orderStatusText(LitemallOrder order) {
        Short status = order.getOrderStatus();

        if (status.equals(STATUS_CREATE)) {
            return "未付款";
        }
        if (status.equals(STATUS_CANCEL)) {
            return "已取消";
        }
        if (status.equals(STATUS_AUTO_CANCEL)) {
            return "已取消(系统)";
        }
        if (status.equals(STATUS_ADMIN_CANCEL)) {
            return "已取消(管理员)";
        }
        if (status.equals(STATUS_PAY)) {
            return "已付款";
        }
        if (status.equals(STATUS_REFUND)) {
            return "订单取消，退款中";
        }
        if (status.equals(STATUS_REFUND_CONFIRM)) {
            return "已退款";
        }
        if (status.equals(STATUS_GROUPON_NONE)) {
            return "待开团";
        }
        if (status.equals(STATUS_GROUPON_ON)) {
            return "团购中";
        }
        if (status.equals(STATUS_GROUPON_FAIL)) {
            return "团购失败";
        }
        if (status.equals(STATUS_GROUPON_SUCCEED)) {
            return "团购成功";
        }
        if (status.equals(STATUS_SHIP)) {
            return "已发货";
        }
        if (status.equals(STATUS_CONFIRM)) {
            return "已收货";
        }
        if (status.equals(STATUS_AUTO_CONFIRM)) {
            return "已收货(系统)";
        }
        if (status.equals(STATUS_COMMENT_OVERTIME)) {
            return "评价已超时";
        }
        if (status.equals(STATUS_COMMENT)) {
            return "交易完成";
        }
        if (status.equals(STATUS_PUT_AFTERSALE)) {
            return "售后申请中";
        }
        if (status.equals(STATUS_DISPOSE_AFTERSALE)) {
            return "售后退款中";
        }
        if (status.equals(STATUS_FINISH_AFTERSALE)) {
            return "售后已完成";
        }
        if (status.equals(STATUS_REJECT_AFTERSALE)) {
            return "售后已拒绝";
        }
        throw new IllegalStateException("orderStatus不支持");
    }


    public static OrderHandleOption build(LitemallOrder order) {
        Short status = order.getOrderStatus();
        OrderHandleOption handleOption = new OrderHandleOption();

        if (status.equals(STATUS_CREATE) || status.equals(STATUS_GROUPON_NONE)) {
            // 如果订单没有被取消，且没有支付，则可支付，可取消
            handleOption.setCancel(true);
            handleOption.setPay(true);
        } else if (status.equals(STATUS_CANCEL) || status.equals(STATUS_AUTO_CANCEL)) {
            // 如果订单已经取消或是已完成，则可删除
            handleOption.setDelete(true);
        } else if (status.equals(STATUS_PAY) || status.equals(STATUS_GROUPON_ON) || status.equals(STATUS_GROUPON_SUCCEED)) {
            // 如果订单已付款，没有发货，则可退款
            handleOption.setRefund(true);
        } else if (status.equals(STATUS_REFUND) || status.equals(STATUS_GROUPON_FAIL)) {
            // 如果订单申请退款中，没有相关操作
        } else if (status.equals(STATUS_REFUND_CONFIRM)) {
            // 如果订单已经退款，则可删除
            handleOption.setDelete(true);
        } else if (status.equals(STATUS_SHIP)) {
            // 如果订单已经发货，没有收货，则可收货操作,此时不能取消订单
            handleOption.setConfirm(true);
        } else if (status.equals(STATUS_CONFIRM) || status.equals(STATUS_AUTO_CONFIRM)) {
            // 如果订单已经支付，且已经收货，则可删除、去评论、申请售后和再次购买
            handleOption.setDelete(true);
            handleOption.setComment(true);
            handleOption.setRebuy(true);
            handleOption.setAftersale(true);
        }else if (status.equals(STATUS_COMMENT_OVERTIME)) {
            // 评论超时禁止评论
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
            handleOption.setAftersale(true);
        }else if (status.equals(STATUS_COMMENT)) {
            // 如果订单已经评论，则完成交易
            handleOption.setDelete(true);
            handleOption.setRebuy(true);
            handleOption.setAftersale(true);
        }else if (status.equals(STATUS_PUT_AFTERSALE)) {
            // 售后申请中
        }else if (status.equals(STATUS_DISPOSE_AFTERSALE)) {
            // 售后处理中
        }else if (status.equals(STATUS_FINISH_AFTERSALE)) {
            // 售后已完成
            handleOption.setDelete(true);
        }else if (status.equals(STATUS_REJECT_AFTERSALE)) {
            // 售后已拒绝
            handleOption.setDelete(true);
        }  else {
            throw new IllegalStateException("status不支持");
        }

        return handleOption;
    }

    public static List<Short> orderStatus(Integer showType) {
        // 全部订单
        if (showType.equals(0)) {
            return null;
        }
        List<Short> status = new ArrayList<>();
        if (showType.equals(1)) {
            // 待付款订单
            status.add(STATUS_CREATE);
            status.add(STATUS_GROUPON_NONE);
        } else if (showType.equals(2)) {
            // 待发货订单
            status.add(STATUS_PAY);
            status.add(STATUS_GROUPON_ON);
            status.add(STATUS_GROUPON_SUCCEED);
        } else if (showType.equals(3)) {
            // 待收货订单
            status.add(STATUS_SHIP);
        } else if (showType.equals(4)) {
            // 待评价订单
            status.add(STATUS_CONFIRM);
            status.add(STATUS_AUTO_CONFIRM);
            status.add(STATUS_COMMENT);
            status.add(STATUS_COMMENT_OVERTIME);
        } else {
            return null;
        }
        return status;
    }

    /**判断是否可以支付*/
    public static boolean hasPayed(LitemallOrder order) {
        return  OrderUtil.isPayStatus(order) || OrderUtil.isCancelStatus(order) ||
                OrderUtil.isAutoCancelStatus(order);
    }
    /**未付款*/
    public static boolean isCreateStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_CREATE.equals(litemallOrder.getOrderStatus());
    }
    /**已取消*/
    public static boolean isCancelStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_CANCEL.equals(litemallOrder.getOrderStatus());
    }
    /**已取消（系统）*/
    public static boolean isAutoCancelStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_AUTO_CANCEL.equals(litemallOrder.getOrderStatus());
    }
    /**已付款*/
    public static boolean isPayStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_PAY.equals(litemallOrder.getOrderStatus());
    }
    /**订单取消，退款中*/
    public static boolean isRefundStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_REFUND.equals(litemallOrder.getOrderStatus());
    }
    /**已退款*/
    public static boolean isRefundConfirmStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_REFUND_CONFIRM.equals(litemallOrder.getOrderStatus());
    }
    /**待开团（未支付）*/
    public static boolean isGrouponNoneStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_GROUPON_NONE.equals(litemallOrder.getOrderStatus());
    }
    /**团购中（已支付）*/
    public static boolean isGrouponOnStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_GROUPON_ON.equals(litemallOrder.getOrderStatus());
    }
    /**团购失败（待退款）*/
    public static boolean isGrouponFailStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_GROUPON_FAIL.equals(litemallOrder.getOrderStatus());
    }
    /**团购成功（待发货）*/
    public static boolean isGrouponSucceedStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_GROUPON_SUCCEED.equals(litemallOrder.getOrderStatus());
    }
    /**已发货*/
    public static boolean isShipStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_SHIP.equals(litemallOrder.getOrderStatus());
    }
    /**已收货*/
    public static boolean isConfirmStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_CONFIRM.equals(litemallOrder.getOrderStatus());
    }
    /**已收货（系统）*/
    public static boolean isAutoConfirmStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_AUTO_CONFIRM.equals(litemallOrder.getOrderStatus());
    }
    /**评论已超时*/
    public static boolean isCommentOvertimeStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_COMMENT_OVERTIME.equals(litemallOrder.getOrderStatus());
    }
    /**交易完成*/
    public static boolean isCommentStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_COMMENT.equals(litemallOrder.getOrderStatus());
    }
    /**售后申请中*/
    public static boolean isPutAftersaleStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_PUT_AFTERSALE.equals(litemallOrder.getOrderStatus());
    }
    /**售后退款中*/
    public static boolean isDisposeAftersaleStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_DISPOSE_AFTERSALE.equals(litemallOrder.getOrderStatus());
    }
    /**售后已完成*/
    public static boolean isFinishAftersaleStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_FINISH_AFTERSALE.equals(litemallOrder.getOrderStatus());
    }
    /**售后已拒绝*/
    public static boolean isRejectAftersaleStatus(LitemallOrder litemallOrder) {
        return OrderUtil.STATUS_REJECT_AFTERSALE.equals(litemallOrder.getOrderStatus());
    }


}
