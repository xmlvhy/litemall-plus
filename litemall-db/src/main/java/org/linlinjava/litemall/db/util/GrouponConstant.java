package org.linlinjava.litemall.db.util;

import org.linlinjava.litemall.db.domain.LitemallGroupon;

public class GrouponConstant {
    /**团购规则正常*/
    public static final Short RULE_STATUS_ON = 0;
    /**团购规则活动取消*/
    public static final Short RULE_STATUS_DOWN_EXPIRE = 1;
    /**团购规则已下线*/
    public static final Short RULE_STATUS_DOWN_ADMIN = 2;

    /**团购取消（未支付）*/
    public static final Short STATUS_CANCEL = -1;
    /**待开团（未支付）*/
    public static final Short STATUS_NONE = 0;
    /**团购中（已支付）*/
    public static final Short STATUS_ON = 1;
    /**团购失败（待退款）*/
    public static final Short STATUS_FAIL = 2;
    /**团购成功（待发货）*/
    public static final Short STATUS_SUCCEED = 3;

    public static String GrouponStatusText(LitemallGroupon groupon) {
        Short status = groupon.getStatus();

        if (status.equals(STATUS_CANCEL)) {
            return "取消团购";
        }
        if (status.equals(STATUS_NONE)) {
            return "待开团";
        }
        if (status.equals(STATUS_ON)) {
            return "团购中";
        }
        if (status.equals(STATUS_FAIL)) {
            return "团购失败";
        }
        if (status.equals(STATUS_SUCCEED)) {
            return "团购成功";
        }
        throw new IllegalStateException("grouponStatus不支持");
    }
}
