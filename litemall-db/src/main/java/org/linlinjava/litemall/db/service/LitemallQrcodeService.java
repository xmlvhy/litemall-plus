package org.linlinjava.litemall.db.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallQrcodeMapper;
import org.linlinjava.litemall.db.domain.LitemallQrcode;
import org.linlinjava.litemall.db.domain.LitemallQrcodeExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 燃烧的头发
 * @since 2022-01-20
 */
@Service
public class LitemallQrcodeService extends ServiceImpl<LitemallQrcodeMapper, LitemallQrcode> {

	@Resource
	private LitemallQrcodeMapper qrcodeMapper;

	public Integer add(LitemallQrcode qrcode) {
		qrcode.setId(0);
		qrcode.setAddTime(LocalDateTime.now());
		qrcode.setUpdateTime(LocalDateTime.now());
		return qrcodeMapper.insertSelective(qrcode);
	}

	public int count() {
		LitemallQrcodeExample example = new LitemallQrcodeExample();
		example.or().andDeletedEqualTo(false);
		return (int) qrcodeMapper.countByExample(example);
	}

	public List<LitemallQrcode> findQrcode(String tableNo) {
		LitemallQrcodeExample example = new LitemallQrcodeExample();
		example.or().andTableNoEqualTo(tableNo).andDeletedEqualTo(false);
		return qrcodeMapper.selectByExample(example);
	}

	public List<LitemallQrcode> queryList(Integer page, Integer limit, String sort, String order) {
		LitemallQrcodeExample example = new LitemallQrcodeExample();
		example.or().andDeletedEqualTo(false);

		if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
			example.setOrderByClause(sort + " " + order);
		}
		PageHelper.startPage(page, limit);
		return qrcodeMapper.selectByExampleWithBLOBs(example);
	}

	public int updateBlobById(LitemallQrcode qrcode) {
		qrcode.setUpdateTime(LocalDateTime.now());
		return qrcodeMapper.updateByPrimaryKeyWithBLOBs(qrcode);
	}


	/**
	 * 直接删除不逻辑删除
	 * @param id
	 * @return
	 */
	public int deleteById(Integer id) {
		return qrcodeMapper.deleteById(id);
	}

}
